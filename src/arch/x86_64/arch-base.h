#ifndef _ARCH_SPECIFIC_H
#define _ARCH_SPECIFIC_H

/*
 * Kernel can clobber rcx and r11 while serving a syscall, those are ignored
 * The layout of this struct depends on the way the assembly wrapper saves
 * register on the stack.
 * Note: don't expect the SIMD array to be aligned for efficient use with
 * AVX instructions.
 */
struct sys_ctx {
	long rip;
	long r15;
	long r14;
	long r13;
	long r12;
	long r10;
	long r9;
	long r8;
	long rsp;
	long rbp;
	long rdi;
	long rsi;
	long rbx;
	long rdx;
	long rax;
	char padd[0x200 - 0x168]; /* see: stack layout in intercept_wrapper.s */
	long SIMD[16][8]; /* 8 SSE, 8 AVX, or 16 AVX512 registers */
};

#endif
